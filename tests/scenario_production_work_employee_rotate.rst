========================================
Production Work Employee Rotate Scenario
========================================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta

Install production_work_employee_rotate::

    >>> config = activate_modules('production_work_employee_rotate')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

create work centers::

    >>> WorkCenter = Model.get('production.work.center')
    >>> WorkCenterCategory = Model.get('production.work.center.category')
    >>> category1 = WorkCenterCategory(name='Category 1')
    >>> category1.save()
    >>> center_root = WorkCenter(name='Root')
    >>> center_root.save()
    >>> center1 = WorkCenter(name='Center 1',
    ...     parent=center_root, category=category1, sequence=1)
    >>> center1.save()
    >>> center2 = WorkCenter(name='Center 2',
    ...     parent=center_root, category=category1, sequence=2)
    >>> center2.save()
    >>> center3 = WorkCenter(name='Center 3',
    ...     parent=center_root, category=category1, sequence=3)
    >>> center3.save()
    >>> center4 = WorkCenter(name='Center 4',
    ...     parent=center_root, category=category1, many_employees=True,
    ...     sequence=4)
    >>> center4.save()

Create employees::

    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> party1 = Party(name='Employee 1', code='00001')
    >>> party1.save()
    >>> party2 = Party(name='Employee 2', code='00002')
    >>> party2.save()
    >>> party3 = Party(name='Employee 3', code='00003')
    >>> party3.save()
    >>> party4 = Party(name='Employee 4', code='00004')
    >>> party4.save()
    >>> party5 = Party(name='Employee 5', code='00005')
    >>> party5.save()
    >>> employee1 = Employee(party=party1,company=company)
    >>> employee1.save()
    >>> ewc = employee1.work_centers.new()
    >>> ewc.date = datetime.date.today()
    >>> ewc.end_date = datetime.date.today() + relativedelta(days=1)
    >>> ewc.work_center = center1
    >>> employee1.save()
    >>> employee2 = Employee(party=party2,company=company)
    >>> employee2.save()
    >>> ewc = employee2.work_centers.new()
    >>> ewc.date = datetime.date.today()
    >>> ewc.work_center = center2
    >>> employee2.save()
    >>> employee3 = Employee(party=party3,company=company)
    >>> employee3.save()
    >>> ewc = employee3.work_centers.new()
    >>> ewc.date = datetime.date.today()
    >>> ewc.work_center = center3
    >>> employee3.save()
    >>> employee4 = Employee(party=party4,company=company)
    >>> ewc = employee4.work_centers.new()
    >>> ewc.date = datetime.date.today()
    >>> ewc.work_center = center4
    >>> employee4.save()
    >>> employee5 = Employee(party=party5,company=company)
    >>> employee5.save()
    >>> ewc = employee5.work_centers.new()
    >>> ewc.date = datetime.date.today() - relativedelta(days=2)
    >>> ewc.work_center = center1
    >>> employee5.save()
    >>> EmployeeWorkcenter = Model.get('company.employee-work.center')
    >>> ewcenters = EmployeeWorkcenter.find([])

Rotate employees::

    >>> rotate = Wizard('production.work.center.employee_rotate', [center_root])
    >>> rotate.form.date_ != None
    True
    >>> rotate.form.mode
    'round-robin'
    >>> rotate.form.positions = 2
    >>> rotate.execute('edit_')
    >>> len(rotate.form.work_centers)
    3
    >>> len(rotate.form.work_centers[0].dates)
    1
    >>> for x, wc in enumerate(rotate.form.work_centers):
    ...     for y, _day in enumerate(wc.dates):
    ...         _day.rotated_employee == rotate.form.work_centers[
    ...             (x - 2 + (y * -2)) % len(rotate.form.work_centers)].init_employee
    True
    True
    True
    >>> rotate.execute('rotate') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ('UserError', ('The work center can only have an employee in a day.', ''))
    >>> rotate.execute('start')
    >>> rotate.form.positions = 2
    >>> rotate.form.end_date = datetime.date.today() + relativedelta(days=1)
    >>> rotate.execute('edit_')
    >>> len(rotate.form.work_centers)
    3
    >>> for x, wc in enumerate(rotate.form.work_centers):
    ...     for y, _day in enumerate(wc.dates):
    ...         _day.rotated_employee == rotate.form.work_centers[
    ...             (x - 2 + (y * -2)) % len(rotate.form.work_centers)].init_employee
    ...         _day.start_date == rotate.form.date_
    ...         _day.end_date == rotate.form.end_date
    True
    True
    True
    True
    True
    True
    True
    True
    True
    >>> rotate.execute('rotate')
    >>> new_ewcenters = EmployeeWorkcenter.find([
    ...     ('id', 'not in', [ew.id for ew in ewcenters])])
    >>> len(new_ewcenters)
    3
    >>> for ewcenter in new_ewcenters:
    ...     ewcenter.delete()

Rotate employees with daily repeating::

    >>> rotate = Wizard('production.work.center.employee_rotate', [center_root])
    >>> rotate.form.positions = 1
    >>> rotate.form.mode = 'round-robin'
    >>> rotate.form.repeat_ = 'daily'
    >>> rotate.form.end_date = datetime.date.today() + relativedelta(days=3)
    >>> rotate.execute('edit_')
    >>> len(rotate.form.work_centers)
    3
    >>> len(rotate.form.work_centers[0].dates)
    4
    >>> for x, wc in enumerate(rotate.form.work_centers):
    ...     for y, _day in enumerate(wc.dates):
    ...         _day.rotated_employee == rotate.form.work_centers[
    ...             (x - 1 + (y * -1)) % len(rotate.form.work_centers)].init_employee
    True
    True
    True
    True
    True
    True
    True
    True
    True
    True
    True
    True
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> tomorrow2 = today + relativedelta(days=2)
    >>> tomorrow3 = today + relativedelta(days=3)
    >>> ([(_date.start_date, _date.end_date)
    ...     for _date in rotate.form.work_centers[0].dates] == [
    ...     (today, today), (tomorrow, tomorrow), (tomorrow2, tomorrow2),
    ...         (tomorrow3, tomorrow3)])
    True
    >>> rotate.execute('rotate')
    >>> new_ewcenters = EmployeeWorkcenter.find([
    ...     ('id', 'not in', [ew.id for ew in ewcenters])])
    >>> len(new_ewcenters)
    12
